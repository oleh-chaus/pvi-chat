require('dotenv').config();

const cors = require('cors');
const express = require('express');
const mongoose = require('mongoose');
const session = require('express-session');
const socketIO = require('socket.io');

const users = require('./routes/users');
const messages = require('./routes/messages');
const tasks = require('./routes/tasks');

const mongoString = process.env.DATABASE_URL;

mongoose.connect(mongoString);
const db = mongoose.connection;

db.on('error', (error) => console.log(error));

db.once('connected', () => console.log('Connected to database!'));

const app = express();

const corsMiddleware = cors({
  credentials: true,
  origin: 'http://localhost:5173'
});

app.use(corsMiddleware);

const sessionMiddleware = session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  cookie: {
    secure: false,
    maxAge: 1000 * 60 * 60 * 24 * 7, // 1 week
    httpOnly: false
  }
});

app.use(sessionMiddleware);

app.use(express.json());
app.use('/users', users);
app.use('/messages', messages);
app.use('/tasks', tasks);

const server = app.listen(3000, () => {
  console.log('Server started');
});

const io = socketIO(server, {
  cors: {
    credentials: true,
    origin: 'http://localhost:5173'
  }
});

io.engine.use(sessionMiddleware);
const connectedUsers = {};


io.on('connection', (socket) => {

  if (!socket.request.session.username) {
    console.log('🚫: Unauthorized connection!');
    socket.disconnect();
    return;
  }

  const username = socket.request.session.username;
  console.log(`⚡: ${username} just connected!`);
  connectedUsers[username] = socket;

  socket.on('message', (data) => {

    const { receiver, message, global } = data;
    const receiverSocket = connectedUsers[receiver];
    if (global) {
      console.log(`📩: ${username} just sent a global message!`);
      console.log(data);

      socket.broadcast.emit('message', {
        sender: username,
        message: message,
        global: true,
        date: Date.now()
      });
      return;
    }

    console.log(`📩: ${username} just sent a message to ${receiver}!`);
    console.log(data);


    if (receiverSocket) {
      receiverSocket.emit('message', {
        sender: username,
        message: message,
        date: Date.now()
      });
    }


  });

  socket.on('disconnect', () => {
    console.log(`💡: ${username} just disconnected!`);
  });
});
