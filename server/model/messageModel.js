const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
    sender: {
        type: String,
        required: true
    },
    receiver: {
        type: String,
        required: false
    },
    message: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: false
    },
    global: {
        type: Boolean,
        required: false
    }
});

module.exports = mongoose.model("Message", messageSchema);