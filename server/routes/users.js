const bcrypt = require('bcrypt');
const saltRounds = 10;
const express = require('express');
const router = express.Router();
const User = require('../model/userModel');

module.exports = router;

// register
router.post('/register', async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ username });
        //console.log(user);
        const hashedPassword = await bcrypt.hash(password, saltRounds);
        const data = new User({
            username: username,
            password: hashedPassword
        });
        const savedData = await data.save();
        res.status(200).json(savedData);
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
});

router.post('/login', async (req, res) => {
    try {
        const { username, password } = req.body;
        if (!username || !password) {
            res.status(400).json({ message: 'Username and password are required.' });
            return;
        }
        const user = await User.findOne({ username });
        if (!user) {
            res.status(400).json({ message: 'Username or password is incorrect.' });
            return;
        }
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            res.status(400).json({ message: 'Username or password is incorrect.' });
            return;
        }
        req.session.userId = user._id;
        req.session.username = user.username;
        req.session.save();

        res.status(200).json({ message: 'Login successful.', username: user.username });
        //console.log(req.session);
    }
    catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.post('/logout', async (req, res) => {
    try {
        if (!req.session.userId) {
            res.status(401).json({ message: 'You are not logged in.' });
            return;
        }
        req.session.destroy();
        res.clearCookie('connect.sid'); // Clear the session cookie
        res.status(200).json({ message: 'Logout successful.' });
    }
    catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/search', async (req, res) => {
    console.log("searching");
    const { query } = req.query;

    try {
        if (!req.session.userId) {
            res.status(401).json({ message: 'You are not logged in.' });
            return;
        }
        const currentUser = req.session.username;
        let queryCondition = {};

        if (query) {
            queryCondition = {
                $and: [
                    {
                        username: {
                            $regex: "^" + query,
                        },
                    },
                    {
                        username: {
                            $ne: currentUser
                        }
                    }
                ]
            };
        } else {
            queryCondition = {
                username: {
                    $ne: currentUser
                }
            };
        }

        const users = await User.find(queryCondition).select('username');
        res.status(200).json(users);
    }
    catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/checkLogin', async (req, res) => {
    // console.log(req.session);
    // console.log(req.session.username);
    try {
        if (!req.session.userId) {
            res.status(401).json({
                message: 'You are not logged in.',
                username: req.session.username
            });
            return;
        }
        res.status(200).json({ message: 'You are logged in.' });
    }
    catch (error) {
        res.status(400).json({ message: error.message });
    }
});