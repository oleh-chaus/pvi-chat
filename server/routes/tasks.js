const express = require('express');
const router = express.Router();
const Task = require('../model/taskModel');

module.exports = router;

router.get('/fetchTasks', async (req, res) => {
    try {
        if (!req.session.userId) {
            res.status(401).json({ message: 'You are not logged in.' });
            return;
        }

        const username = req.session.username;

        const tasks = await Task.find({ username: username });

        res.status(200).json(tasks);
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
});

router.post('/add', async (req, res) => {
    try {
        if (!req.session.userId) {
            res.status(401).json({ message: 'You are not logged in.' });
            return;
        }

        const task = new Task({
            name: req.body.name,
            username: req.session.username,
            status: req.body.status,
            deadline: req.body.deadline,
            description: req.body.description
        });

        const savedTask = await task.save();
        res.status(200).json(savedTask);
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
});

router.put('/edit', async (req, res) => {
    try {
        if (!req.session.userId) {
            res.status(401).json({ message: 'You are not logged in.' });
            return;
        }

        const { _id, status, name, deadline, description } = req.body;

        const filter = { _id: _id };
        const update = { 
            status: status,
            name: name,
            deadline: deadline,
            description: description
        };
        const data = await Task.findOneAndUpdate(filter, update, { new: true });

        res.status(200).json(data);
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
});

router.delete('/delete', async (req, res) => {
    try {
        if (!req.session.userId) {
            res.status(401).json({ message: 'You are not logged in.' });
            return;
        }

        const { _id } = req.body;

        const filter = { _id: _id };

        const data = await Task.findOneAndDelete(filter);
        res.status(200).json(data);
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
});