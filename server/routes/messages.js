const express = require('express');
const router = express.Router();
const Message = require('../model/messageModel');
module.exports = router;


// POST
router.post('/send', async (req, res) => {
    try {
        if(!req.session.userId) {
            res.status(401).json({message: 'You are not logged in.'});
            return;
        }
        if(req.body.receiver === req.session.username) {
            res.status(400).json({message: 'You cannot send a message to yourself.'});
            return;
        }
        const data = new Message({
            sender: req.session.username,
            receiver: req.body.receiver,
            message: req.body.message,
            global: req.body.global,
            date: Date.now()
        });
        
        const savedData = await data.save();
        res.status(200).json(savedData);
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
});

router.get('/fetchMessages', async (req, res) => {
    try {
        if(!req.session.userId) {
            res.status(401).json({message: 'You are not logged in.'});
            return;
        }
        const username = req.session.username;

        const data = await Message.find({
            $or: [
                {sender: username},
                {receiver: username},
                {global: true}
            ]
        }).sort({date: -1});

        //console.log(data);
        
        res.status(200).json({messages: data, username: username});
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
});