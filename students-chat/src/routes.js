import Login from "./components/Login.vue";
import Chat from "./components/Chat.vue";
import Todo from "./components/Todo.vue";
const routes = [
    {
        path: '/login',
        component: Login,
        name: 'Login',
    },
    {
        path: '/chat',
        component: Chat,
        meta: {
            requiresAuth: true
        },
        name: 'Chat',
    },
    {
        path: '/',
        redirect: '/chat'
    },
    {
        path: '/todo',
        component: Todo,
        meta: {
            requiresAuth: true
        },
        name: 'Todo',
    }
]

export default routes;