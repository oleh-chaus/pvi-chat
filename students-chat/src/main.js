import axios from 'axios'
axios.defaults.withCredentials = true;
import io from 'socket.io-client';
import { createRouter, createWebHistory } from 'vue-router'
import { createApp } from 'vue'
import App from './App.vue'

import "bootstrap/dist/css/bootstrap.min.css"
import 'bootstrap-icons/font/bootstrap-icons.css'

import routes from './routes.js'

const router = createRouter({
    history: createWebHistory(),
    routes,
});


router.beforeEach(async (to, from) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        try {
            const response = await axios.get('http://localhost:3000/users/checkLogin')
            if (response.status === 200) {
                return true
            } else {
                return { name: 'Login' }
            }
        } catch (error) {
            return { name: 'Login' }
        }
    } else {
        return true
    }
})



const app = createApp(App);



let socket = null;
app.config.globalProperties.initialiseSocketConnection = () => {
    app.config.globalProperties.socket = io('http://localhost:3000', {
        withCredentials: true,
    });
    console.log(socket);
};
app.config.globalProperties.socket = socket;

console.log(app.config.globalProperties)

app.use(router);
app.mount('body');
import "bootstrap/dist/js/bootstrap.min.js"
